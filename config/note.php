<?php

// class that descripes each note
class Note
{
    public $id;
    public $title;
    public $text;
    public $date;

    // Fills in all entry parameters when creating new one
    public function __construct($row)
    {
        $this->id = $row["ID"];
        $this->title = $row["Title"];
        $this->text = $row["Text"];
        $this->date = $row["Date"];
    }
    
    // Change property value
    function __set($property, $value)
    {
        $this->$property = $value;
    }

    // Get date in user friendly format
    private function getDate()
    {
        $today = date("Ymd");
        $postDate = date("Ymd", $this->date);
        $daysPassed = $today - $postDate; // get number of days passed since note creating/editing

        switch ($daysPassed) {
            case 0:
                $when = "šodien";
                break;
            case 1:
                $when = "vakar";
                break;
            case ($daysPassed < 21):
                $when = "pirms " . $daysPassed . " dienām";
                break;
            default:
                $when = date("d.m.Y", $this->date);
        }
        return $when;
    }

    // Display note on page in user friendly format
    public function display()
    {
        echo '<div class="container border border-secondary shadow mb-4">
            <form action="edit.php" method="POST" class="p-4">
                <div class="row">
                    <div class="col-10">
                        <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" data-toggle="tooltip" title="Check if completed" id="checkbox">
                            <h5>' . $this->title . '</h5>
                        </label>
                    </div>
                    <div class="col-2">
                        <input type="hidden" name="id" value="' . $this->id . '">
                        <input type="submit" class="float-right btn btn-secondary pl-4 pr-4" value="Labot">
                    </div>
                </div>
                <div class="row">
                    <div class="col-9">
                        <p>' . $this->text . '</p>
                    </div>
                    <div class="col-3 mt-3 align-bottom">Pēdējas izmaiņas- ' . date("d.m.Y", $this->date) . '</div>
                </div>
            </form>
        </div>'; //unknown mistake-error (it should be "getDate()" on place of "date("d.m.Y", $this->date)"
    }
}
?>