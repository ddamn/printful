<?php
require_once 'note.php';
class dbConnection
{
    public $notes = array();
    protected $servername = "localhost";
    protected $username = "root";
    protected $password = "";
    protected $dbname = "printful";

    private function __construct()
    {
        // Create connection
        $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
        // Check connection
        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
    }
    
    private function __distruct() {
        $this->conn->close();
    }
     
    public function selectAll()
    {
        // sql to select all records
        $sql = "SELECT * FROM notes ORDER BY Date DESC";
        $result = $this->conn->query($sql);

        // Exctracts data from each row
        while($row = $result->fetch_assoc()) {
            $this->notes[$row["ID"]] = new Note($row);
        }
    }
    
    


    // Insert new entry into database
    function insert($note)
    {
        // sql to insert new record
        $sql = "INSERT INTO notes (Title, Text, Date)
            VALUES ('".$note->title."', '".$note->text."', ".$note->date.")";

            if ($this->conn->query($sql) === TRUE) {
                echo "<script>alert('Jauna piezīme sekmīgi pievienota');</script>";
            } else {
                echo "<script>alert('Error: " . $sql . "<br>" . $this->conn->error . "');</script>";
            }
    }


    // Update entry in database
    function update($note)
    {
        // sql to update a record
        $sql = "UPDATE notes SET Title='" . $note->title . "', Text='" . $note->text . "', Date=" . $note->date . " WHERE ID=" . $note->id;

        if ($this->conn->query($sql) === TRUE) {
            echo "<script>alert('Piezīme sekmīgi rediģēta');</script>";
        } else {
            echo "<script>alert('Error: " . $sql . "<br>" . $this->conn->error . "');</script>";
        }
    }


    // Delete entry from database
    function del($note)
    {
        // sql to delete a record
        $sql = "DELETE FROM notes WHERE ID=" . $note->id;

        if ($this->conn->query($sql) === TRUE) {
            echo "<script>alert('Piezīme sekmīgi izdzēsta');</script>";
        } else {
            echo "<script>alert('Error: " . $sql . "<br>" . $this->conn->error ."');</script>";
        }
    }
}
?>