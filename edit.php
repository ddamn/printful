<?php
    require_once 'config/data.php';
    $current = $_POST['id'];
    $title = $notes[$current]->title;
    $text = $notes[$current]->text;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Labot</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container jumbotron text-center">
  <h1 class="text-muted">Darāmo lietu saraksts</h1>
  <h4>Labot</h4>
</div>

<div class="container shadow mb-4">
    <form method="POST" class="needs-validation p-4" novalidate>
    <input type="hidden" name="id" value="<?php echo $current?>">
        <div class="form-group">
            <label for="title">Virsraksts:</label>
            <input type="text" class="form-control text-body" placeholder="Virsraksts" id="title" name="title" value="<?php echo $title; ?>" required>
            <div class="valid-feedback"></div>
            <div class="invalid-feedback">Obligāts aizpildīšanai laukums</div>
        </div>
        <div class="form-group">
            <label for="comment">Apraksts:</label>
            <textarea class="form-control text-body" placeholder="Apraksts" rows="5" id="comment" name="comment"><?php echo $text; ?></textarea>
        </div>
        <div class="row">
            <div class="col-5">
                <a href="index.php" class="float-left btn btn-dark shadow pl-5 pr-5 ml-3" role="button">Doties atpakaļ</a>
            </div>
            <div class="col-4">
                <button type="submit" class="btn btn-danger shadow pl-5 pr-5" name="delete" formnovalidate>Dzēst</button>
            </div>
            <div class="col-3">
                <input type="submit" class="float-right btn btn-success shadow pl-5 pr-5 mr-3" name="submit" value="Labot">
            </div>
        </div> 
    </form>
</div>
<script src="js/script.js"></script>
</body>
</html>
<?php
$current = $_POST['id'];

// Change existing and ubdate its entry in database
if(isset($_POST['submit'])){
    $notes[$current]->__set('title', $_POST['title']);
    $notes[$current]->__set('text', $_POST['comment']);
    $notes[$current]->__set('date', time());
    update($notes[$current]);
    header('Location: index.php');
} elseif(isset($_POST['delete'])){
    del($notes[$current]);
    unset($notes[$current]);
    header('Location: index.php');
}
    
?>