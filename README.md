
@ Rolands Kalituha


# Printful

Web page created as test assigment for pritful php summerschool.
To view page without hosting it please open [this](http://progvaltesti-181rdc049.epizy.com/printful/index.php) page.

## Configurations

Create new database with following parameters:

```php
	$servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "printful";
```
Use sql from directory DatabaseTable/notes.sql to create table named "notes" 

## Usage

Host all files from represitory, configurate database and table, open index.php on your server

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Notification about errors and mistakes
File config/dbConnection.php was created as a class for connection with database.
But its throws an error, so was replaced with config/data.php to allow website work.
config/data.php doesn't follows oop principles.

also there is a mistake with using getDate() method in Note class.

Because of lack of knowlage would like to ask for a help to solve those problems.