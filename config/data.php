<?php
require_once 'note.php';

$notes = array();
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "printful";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * FROM notes ORDER BY Date DESC";
$result = $conn->query($sql);

// Exctracts data from each row
while($row = $result->fetch_assoc()) {
    $notes[$row["ID"]] = new Note($row);
}
$conn->close();


// Insert new entry into database
function insert($note)
{
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "printful";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "INSERT INTO notes (Title, Text, Date)
        VALUES ('".$note->title."', '".$note->text."', ".$note->date.")";

        if ($conn->query($sql) === TRUE) {
            echo "<script>alert('Jauna piezīme sekmīgi pievienota');</script>";
        } else {
            echo "<script>alert('Error: " . $sql . "<br>" . $conn->error ."');</script>";
        }
    $conn->close();
}


// Update entry in database
function update($note)
{
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "printful";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 

    $sql = "UPDATE notes SET Title='" . $note->title . "', Text='" . $note->text . "', Date=" . $note->date . " WHERE ID=" . $note->id;

    if ($conn->query($sql) === TRUE) {
        echo "<script>alert('Piezīme sekmīgi rediģēta');</script>";
    } else {
        echo "<script>alert('Error: " . $sql . "<br>" . $conn->error ."');</script>";
    }

    $conn->close();
}


// Delete entry from database
function del($note)
{
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "printful";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 

    // sql to delete a record
    $sql = "DELETE FROM notes WHERE ID=" . $note->id;

    if ($conn->query($sql) === TRUE) {
        echo "<script>alert('Piezīme sekmīgi izdzēsta');</script>";
    } else {
        echo "<script>alert('Error: " . $sql . "<br>" . $conn->error ."');</script>";
    }

    $conn->close();
}

?>