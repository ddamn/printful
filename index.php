<?php
    require_once 'config/data.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Daramo saraksts</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container jumbotron text-center">
  <h1>Darāmo lietu saraksts</h1>
</div>

<?php
//display each entry from database
    foreach($notes as $note){
        $note->display();
    }
?>

<div class="container">
    <form action="create.php">
        <button type="submit" class="float-right btn btn-dark shadow pl-5 pr-5 mb-5">Pievienot jaunu</button>
    </form>
</div>
<script src="js/script.js"></script>
</body>
</html>